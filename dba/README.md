# DBA exercise

Initizalize 

```sh
cd dba
docker-compose up
```

Connect to the `interview` database:

```
docker-compose exec postgres psql -U postgres interview
```

Monitor state of the download and database restore:

```sh
docker-compose logs -f load
```

Starting from scratch:

```sh
docker-compose down && docker volume rm dba_postgres_volume && docker-compose up
```